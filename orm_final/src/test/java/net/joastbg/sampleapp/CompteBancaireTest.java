package net.joastbg.sampleapp;

import junit.framework.Assert;
import net.joastbg.sampleapp.entities.CompteBancaire;
import org.junit.Test;

public class CompteBancaireTest {

    @Test
    public void isPrincipal() { //test pour savoir si le compte est le compte principal
        CompteBancaire cb = new CompteBancaire();
        cb.setPrincipal(true);
        Assert.assertTrue(cb.getPrincipal());
        cb.setPrincipal(false);
        Assert.assertFalse(cb.getPrincipal());
    }
}
