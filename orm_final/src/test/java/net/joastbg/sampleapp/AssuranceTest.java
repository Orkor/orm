package net.joastbg.sampleapp;

import net.joastbg.sampleapp.entities.Assurance;
import net.joastbg.sampleapp.entities.AssuranceAuto;
import net.joastbg.sampleapp.entities.AssuranceHabitat;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

public class AssuranceTest {

    @Test
    public void createAssuranceHabitat(){
        Assurance a = new AssuranceHabitat();
        a.setDateSouscription(new Date(118,7,1));
        a.setIdClient(1);
        Assert.assertEquals(new Date(118,7,1), a.getDateSouscription());
        Assert.assertEquals(1, (int) a.getIdClient());
    }

    @Test
    public void createAssuranceAuto(){
        Assurance a = new AssuranceAuto();
        a.setDateSouscription(new Date(118,7,1));
        ((AssuranceAuto) a).setPrincipal(true);
        a.setIdClient(1);
        Assert.assertEquals(new Date(118,7,1), a.getDateSouscription());
        Assert.assertTrue(((AssuranceAuto) a).getPrincipal());
        Assert.assertEquals(1,(int) a.getIdClient());


    }

}
