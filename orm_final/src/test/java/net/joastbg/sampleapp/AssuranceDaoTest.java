package net.joastbg.sampleapp;

import junit.framework.Assert;
import net.joastbg.sampleapp.dao.AssuranceDao;
import net.joastbg.sampleapp.entities.*;
import net.joastbg.sampleapp.exceptions.DaoException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/config/BeanLocations.xml")
public class AssuranceDaoTest {

    @Autowired
    AssuranceDao assuranceDao;

    @Before
    public void setUp() {

    }

    @Test
    public void testPersist(){
        Assurance aa = new AssuranceAuto();
        aa.setDateSouscription(new Date(118,5,11));
        Integer id = assuranceDao.persist(aa);
        Assurance a = assuranceDao.find(id);
        System.out.println("test : " + a.getIdAssurance());
        Assert.assertNotNull("assurance null", a);
    }

    @Test
    public void testResilier(){ //test pour résilier
        Assurance a = (assuranceDao.find(1));
        System.out.println("date : " + a.getEcheancesSet());
        assuranceDao.persist(a);
        Assert.assertNotNull("assurance null", a);
        try {
            assuranceDao.resilier(new Date(119,4,11), a.getIdClient());
            Assert.assertNull("assurance pas null", assuranceDao.find(a.getIdClient()));
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testThreeMonthClientTrue(){//test les clients ayant une date d'anniversaire dans moins de 3 mois
        Assurance aa = new AssuranceAuto();
        aa.setDateSouscription(new Date());
        assuranceDao.persist(aa);

        Assert.assertTrue(threeMonthClient());
    }



    public boolean threeMonthClient(){
        try {
            List<Assurance> a = assuranceDao.findThreeMonthClient();
            for(Assurance date : a){
                Date jour = new Date();
                System.out.println("date : " + date.getDateSouscription());
                if(!(jour.getMonth()==date.getDateSouscription().getMonth()||jour.getMonth()==date.getDateSouscription().getMonth()+1||jour.getMonth()==date.getDateSouscription().getMonth()+2)){
                    return false;
                }
            }
        } catch (DaoException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Test
    public void testfind(){
        Assurance id = assuranceDao.find(2);
        Assert.assertNotNull("Client null", id);
    }



}
