package net.joastbg.sampleapp;

import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.entities.CompteBancaire;
import net.joastbg.sampleapp.entities.PersonneMorale;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClientTest {

    @Test
    public void testAjouterCompteBancaire(){
        Client c = new PersonneMorale();
        CompteBancaire cb1 = new CompteBancaire();
        CompteBancaire cb2 = new CompteBancaire();
        Set<CompteBancaire> cbs = new HashSet<>();
        cbs.add(cb1);
        cbs.add(cb2);
        c.setCompteBancaires(cbs);
        Assert.assertEquals(cbs, c.getCompteBancaires());
    }
}
