package net.joastbg.sampleapp;

import net.joastbg.sampleapp.entities.Echeances;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class EcheancesTest {

    @Test
    public void testCreerEcheance(){
        Echeances e = new Echeances();
        e.setDateEcheance(new Date(118,7,8));
        e.setIdCompte(1);
        e.setMontant(50);
        Assert.assertNotNull(e);
    }
}
