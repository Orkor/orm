package net.joastbg.sampleapp;

import net.joastbg.sampleapp.dao.ClientDao;
import net.joastbg.sampleapp.dao.EcheancesDao;
import net.joastbg.sampleapp.entities.Echeances;
import net.joastbg.sampleapp.exceptions.DaoException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/config/BeanLocations.xml")
public class EcheancesDaoTest {

    @Autowired
    EcheancesDao EcheancesDao;

    @Test
    public void testPersist(){
        Echeances aa = new Echeances();
        aa.setMontant(50);
        Integer id = EcheancesDao.persist(aa);
        Echeances a = EcheancesDao.find(id);
        junit.framework.Assert.assertNotNull("echeance null", a);
    }

    @Test
    public void testNextMonths(){ //test pour trouver dans les 6 prochains mois
        try {
            List<Echeances> list = EcheancesDao.findNextMonths(new Date(118,7,11), 1);
            Assert.assertEquals(6, list.size());
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testFind(){
        Echeances e = EcheancesDao.find(13);
        System.out.println("date : " + e.getidAssurance());
        Assert.assertNotNull(e);
    }


}
