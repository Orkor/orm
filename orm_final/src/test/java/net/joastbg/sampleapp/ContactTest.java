package net.joastbg.sampleapp;

import net.joastbg.sampleapp.entities.Contact;
import org.junit.Assert;
import org.junit.Test;

public class ContactTest {

    @Test
    public void testAjouterContact(){
        Contact c = new Contact();
        c.setType("mail");
        c.setCoordonnees("test@test.com");
        c.setIdClient(1);
        Assert.assertNotNull(c);
        Assert.assertEquals("test@test.com", c.getCoordonnees());
    }
}
