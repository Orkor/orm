
insert into CLIENT(nom) values('supernom');

insert into PERSONNE_MORALE(idClient, siret)
values((SELECT idClient FROM CLIENT WHERE CLIENT.nom = 'supernom'),  'aaab');

insert into CLIENT(nom) values('supernom2');

insert into PERSONNE_PHYSIQUE(idClient, nsecu)
values((SELECT idClient FROM CLIENT WHERE CLIENT.nom = 'supernom2'),  'aaaba');

insert into COMPTE_BANCAIRE(idCompte, iban, principal) values (1, 'ajkzhdjlmkazhd', true);


insert into ASSURANCE(dateSouscription, idClient) values ('2018-05-11', 1);

insert into ASSURANCE_HABITAT(idAssurance) values ((SELECT idAssurance FROM ASSURANCE WHERE ASSURANCE.datesouscription = '2018-05-11'));

insert into ASSURANCE(dateSouscription, idClient) values ('2018-05-12', 2);

insert into ASSURANCE_AUTO(idAssurance, idClient, principal) values ((SELECT idAssurance FROM ASSURANCE WHERE ASSURANCE.datesouscription = '2018-05-12'), 2 , TRUE);

insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-05-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-06-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-07-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-08-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-09-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-10-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-11-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-12-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2019-01-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2019-02-11', 1, 50, 1);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2019-03-11', 1, 50, 1);

insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-05-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-06-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-07-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-08-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-09-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-10-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-11-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2018-12-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2019-01-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2019-02-11', 1, 50, 2);
insert into ECHEANCES(dateEcheance, idCompte, montant, idAssurance) values ('2019-03-11', 1, 50, 2);