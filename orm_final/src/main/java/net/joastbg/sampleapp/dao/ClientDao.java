package net.joastbg.sampleapp.dao;

import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.exceptions.DaoException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClientDao {

    @Autowired
    SessionFactory sessionFactory;

    public Integer persist(Client client){
        Session session = sessionFactory.getCurrentSession();
        Integer returnID = (Integer) session.save(client);
        return returnID;
    }

    public List<Client> findAll(){
        Session session = sessionFactory.getCurrentSession();
        return  session.createQuery("from Client").list();
    }

    public Client findByName(String name) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM Client WHERE nom = :name");
        q.setString("name", name);
        List l = q.list();
        if ( l.size() > 1 ) {
            throw new DaoException("Multiple results");
        } else {
            return (Client) l.get(0);
        }
    }


    public Client find(Integer id){
        Session session = sessionFactory.getCurrentSession();
        return (Client) session.load(Client.class, id);
    }
}
