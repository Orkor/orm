package net.joastbg.sampleapp.dao;

import net.joastbg.sampleapp.entities.Assurance;
import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.exceptions.DaoException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AssuranceDao {

    @Autowired
    SessionFactory sessionFactory;

    public Integer persist(Assurance assurance){
        Session session = sessionFactory.getCurrentSession();
        Integer returnID = (Integer) session.save(assurance);
        return returnID;
    }

    public List<Client> findAll(){
        Session session = sessionFactory.getCurrentSession();
        return  session.createQuery("from Client").list();
    }

    public Assurance findById(Integer id) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM Assurance WHERE idAssurance = :id");
        q.setInteger("id", id);
        List l = q.list();
        if ( l.size() > 1 ) {
            throw new DaoException("Multiple results");
        } else {
            return (Assurance) l.get(0);
        }
    }

    public Assurance find(Integer id){
        Session session = sessionFactory.getCurrentSession();
        return (Assurance) session.get(Assurance.class, id);
    }


    public void resilier(Date date, Integer idClient) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        Query q = session.createQuery("FROM Assurance WHERE idClient = :idClient");
        q.setInteger("idClient", idClient);
        List l = q.list();
        if ( l.size() > 1 ) {
            throw new DaoException("Multiple results");
        }else{
            Assurance a = (Assurance) l.get(0);
            Date dateSouscription = a.getDateSouscription();
            System.out.println("date souscription : " + " " + a.getDateSouscription());
           // dateSouscription.setMonth(dateSouscription.getMonth());
            if(date.getMonth()==dateSouscription.getMonth() && date.getDate()==dateSouscription.getDate()){
                System.out.println("delete " + a.getIdAssurance());
                session.delete(a);
            }
        }

    }

    public List<Assurance> findThreeMonthClient() throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        Date dateDuJour = new Date();
        String dateDuJourString = new SimpleDateFormat("yyyy-MM-dd").format(dateDuJour);
        dateDuJour.setMonth(dateDuJour.getMonth()+3);
        String dateDansTroisMois = new SimpleDateFormat("yyyy-MM-dd").format(dateDuJour);
        System.out.println("QUERY : FROM Assurance WHERE dateSouscription BETWEEN '" +dateDuJourString+"' AND '"+dateDansTroisMois+"'");
        Query q =  session.createQuery("FROM Assurance WHERE dateSouscription BETWEEN '" +dateDuJourString+"' AND '"+dateDansTroisMois+"'");
        List l = q.list();
        if ( l.size() == 0 ) {
            throw new DaoException("No results");
        }else{
         return l;
        }
    }
}
