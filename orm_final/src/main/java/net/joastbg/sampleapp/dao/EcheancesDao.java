package net.joastbg.sampleapp.dao;

import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.entities.Echeances;
import net.joastbg.sampleapp.exceptions.DaoException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class EcheancesDao {
    @Autowired
    SessionFactory sessionFactory;

    public Integer persist(Echeances echeance){
        Session session = sessionFactory.getCurrentSession();
        Integer returnID = (Integer) session.save(echeance);
        return returnID;
    }

    public List<Client> findAll(){
        Session session = sessionFactory.getCurrentSession();
        return  session.createQuery("from Client").list();
    }

    public List<Echeances> findNextMonths(Date date, int idCompte) throws DaoException {
        Session session = sessionFactory.getCurrentSession();
        List<Echeances> list = new ArrayList<>();


        String date1String = new SimpleDateFormat("yyyy-MM-dd").format(date);
        date.setMonth(date.getMonth()+5);
        String date2String = new SimpleDateFormat("yyyy-MM-dd").format(date);
        Query q = session.createQuery("FROM Echeances WHERE dateEcheance between :date1 AND :date2 AND idCompte = :idCompte");
        q.setString("date1", date1String);
        q.setString("date2", date2String);
        q.setInteger("idCompte", idCompte);
        return q.list();
    }

    public Echeances find(Integer id){
        Session session = sessionFactory.getCurrentSession();
        return (Echeances) session.load(Echeances.class, id);
    }



}
