package net.joastbg.sampleapp.entities;


import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="Assurance")
public abstract class Assurance {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="assurance_seq")
    @SequenceGenerator(
            name="assurance_seq",
            sequenceName="assurance_sequence"
    )
    private Integer idAssurance;
    private Date dateSouscription;
    private Integer idClient;


    @OneToMany(mappedBy="idAssurance", cascade = CascadeType.ALL, orphanRemoval=true) //mapping avec les echeances
    private Set<Echeances> echeances;

    public Set<Echeances> getEcheancesSet() {
        return echeances;
    }

    public void setEcheancesSet(Set<Echeances> echeances) {
        this.echeances = echeances;
    }


    public Integer getIdAssurance() {
        return idAssurance;
    }


    public void setIdAssurance(Integer idAssurance) {
        this.idAssurance = idAssurance;
    }

    @Basic
    @Column(name = "datesouscription", nullable = true)
    public Date getDateSouscription() {
        return dateSouscription;
    }

    public void setDateSouscription(Date dateSouscription) {
        this.dateSouscription = dateSouscription;
    }

    @Basic
    @Column(name = "idClient", nullable = true)
    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Assurance assurance = (Assurance) o;
        return Objects.equals(idAssurance, assurance.idAssurance) &&
                Objects.equals(dateSouscription, assurance.dateSouscription) &&
                Objects.equals(idClient, assurance.idClient);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idAssurance, dateSouscription, idClient);
    }
}
