package net.joastbg.sampleapp.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@SequenceGenerator(name = "client_seq")
@Table(name = "PERSONNE_MORALE")
public class PersonneMorale extends Client{
    private String siret;

    @Basic
    @Column(name = "siret", nullable = true, length = 20)
    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonneMorale that = (PersonneMorale) o;
        return Objects.equals(siret, that.siret);
    }

    @Override
    public int hashCode() {

        return Objects.hash(siret);
    }
}
