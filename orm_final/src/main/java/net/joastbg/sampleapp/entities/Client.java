package net.joastbg.sampleapp.entities;


import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Client  {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="client_seq")
    @SequenceGenerator(
            name="client_seq",
            sequenceName="client_sequence"
    )
    private Integer idClient;
    private String nom;


    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "COMPTE_CLIENT", joinColumns = {
            @JoinColumn(name = "idClient", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "idCompte",
                    nullable = false, updatable = false) })
    private Set<CompteBancaire> compteBancaires; //mapping avec les comptes bancaires

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "nom", nullable = true, length = 20)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<CompteBancaire> getCompteBancaires() {
        return compteBancaires;
    }

    public void setCompteBancaires(Set<CompteBancaire> compteBancaires) {
        this.compteBancaires = compteBancaires;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(idClient, client.idClient) &&
                Objects.equals(nom, client.nom);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClient, nom);
    }
}



