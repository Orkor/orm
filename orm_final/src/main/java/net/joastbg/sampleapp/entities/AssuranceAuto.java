package net.joastbg.sampleapp.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@SequenceGenerator(name = "assurance_seq")
@Table(name = "ASSURANCE_AUTO", schema = "", catalog = "")
public class AssuranceAuto extends Assurance{
    private Integer idClient;
    private Boolean principal;

    @Basic
    @Column(name = "idClient", nullable = true, insertable = false, updatable = false)
    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "principal", nullable = true)
    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssuranceAuto that = (AssuranceAuto) o;
        return Objects.equals(idClient, that.idClient) &&
                Objects.equals(principal, that.principal);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idClient, principal);
    }
}
