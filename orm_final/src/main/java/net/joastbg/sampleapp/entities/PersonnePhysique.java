package net.joastbg.sampleapp.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
//@SequenceGenerator(name = "client_seq")
@Table(name = "PERSONNE_PHYSIQUE")
public class PersonnePhysique extends Client{
    private String nsecu;

    @Basic
    @Column(name = "nsecu", nullable = true, length = 20)
    public String getNsecu() {
        return nsecu;
    }

    public void setNsecu(String nsecu) {
        this.nsecu = nsecu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonnePhysique that = (PersonnePhysique) o;
        return Objects.equals(nsecu, that.nsecu);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nsecu);
    }
}
