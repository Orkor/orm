package net.joastbg.sampleapp.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Echeances")
public class Echeances {


    private Integer idEcheance;
    private Integer idCompte;
    private Date dateEcheance;
    private Integer montant;

    @ManyToOne
    @JoinTable(name = "ECHEANCES_ASSURANCE", joinColumns = {
            @JoinColumn(name = "idAssurance", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "idEcheance",
                    nullable = false, updatable = false)})
    private Integer idAssurance; //mapping avec les assurances

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "echeances_seq")
    @SequenceGenerator(
            name = "echeances_seq",
            sequenceName = "echeances_sequence"
    )
    public Integer getIdEcheance() {
        return idEcheance;
    }

    public void setIdEcheance(Integer idEcheance) {
        this.idEcheance = idEcheance;
    }

    @Basic
    @Column(name = "idCompte", nullable = true)
    public Integer getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(Integer idCompte) {
        this.idCompte = idCompte;
    }

    @Basic
    @Column(name = "dateEcheance", nullable = true)
    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    @Basic
    @Column(name = "montant", nullable = true)
    public Integer getMontant() {
        return montant;
    }

    public void setMontant(Integer montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "idAssurance", nullable = true)
    public Integer getidAssurance() {
        return idAssurance;
    }

    public void setidAssurance(Integer idAssurance) {
        this.idAssurance = idAssurance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Echeances echeances = (Echeances) o;
        return Objects.equals(idEcheance, echeances.idEcheance) &&
                Objects.equals(idCompte, echeances.idCompte) &&
                Objects.equals(dateEcheance, echeances.dateEcheance) &&
                Objects.equals(montant, echeances.montant);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idEcheance, idCompte, dateEcheance, montant);
    }

}
