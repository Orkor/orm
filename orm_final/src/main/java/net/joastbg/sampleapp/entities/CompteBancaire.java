package net.joastbg.sampleapp.entities;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "COMPTE_BANCAIRE", schema = "", catalog = "")
public class CompteBancaire {


    private Integer idCompte;
    private String iban;
    private Boolean principal;
    private Integer idClient;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="compte_seq")
    @SequenceGenerator(
            name="compte_seq",
            sequenceName="compte_bancaire_sequence"
    )
    public Integer getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(Integer idCompte) {
        this.idCompte = idCompte;
    }

    @Basic
    @Column(name = "iban", nullable = true, length = 32)
    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Basic
    @Column(name = "principal", nullable = true)
    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    @Basic
    @Column(name = "idClient", nullable = true)
    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompteBancaire that = (CompteBancaire) o;
        return Objects.equals(idCompte, that.idCompte) &&
                Objects.equals(iban, that.iban) &&
                Objects.equals(principal, that.principal) &&
                Objects.equals(idClient, that.idClient);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCompte, iban, principal, idClient);
    }
}
