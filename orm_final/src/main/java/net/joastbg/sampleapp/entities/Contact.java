package net.joastbg.sampleapp.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="contact_seq")
    @SequenceGenerator(
            name="contact_seq",
            sequenceName="contact_sequence"
    )
    private Integer idContact;
    private Integer idClient;
    private String type;
    private String coordonnees;

    public Integer getIdContact() {
        return idContact;
    }

    public void setIdContact(Integer idContact) {
        this.idContact = idContact;
    }

    @Basic
    @Column(name = "idClient", nullable = true)
    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "type", nullable = true, length = 20)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "coordonnees", nullable = true, length = 20)
    public String getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(String coordonnees) {
        this.coordonnees = coordonnees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return Objects.equals(idContact, contact.idContact) &&
                Objects.equals(idClient, contact.idClient) &&
                Objects.equals(type, contact.type) &&
                Objects.equals(coordonnees, contact.coordonnees);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idContact, idClient, type, coordonnees);
    }
}
